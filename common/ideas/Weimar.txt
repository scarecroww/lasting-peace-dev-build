ideas = {
	country = {
		GER_a_country_torn_by_war = {

			picture = GFX_idea_GER_a_country_torn_by_war

			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1
			
			modifier = {
				neutrality_drift = -0.15
				justify_war_goal_time = 0.5
				stability_factor = -0.20
				war_support_factor = -0.25	
			}
		}

		GER_a_new_regime = {

			picture = GFX_idea_GER_a_new_regime

			allowed = {
				always = yes
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1
			
			modifier = {
				socdem_drift = 0.10
				stability_weekly = 0.01	
			}
		}
	}
}